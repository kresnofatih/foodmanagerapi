package tech.kresnofatih.foodmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FoodmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodmanagerApplication.class, args);
	}

}
