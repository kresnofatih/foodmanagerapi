package tech.kresnofatih.foodmanager.constants;

public final class ComparationType {
    public static final String greaterThan = "greaterThan";
    public static final String greaterThanEqual = "greaterThanEqual";
    public static final String lessThan = "lessThan";
    public static final String lessThanEqual = "lessThanEqual";
}
