package tech.kresnofatih.foodmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.constants.ComparationType;
import tech.kresnofatih.foodmanager.exceptions.DrinkNotFoundException;
import tech.kresnofatih.foodmanager.models.Drink;
import tech.kresnofatih.foodmanager.repository.DrinkRepository;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DrinkService {
    private final DrinkRepository drinkRepository;

    @Autowired
    public DrinkService(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    public Drink addDrink(Drink drink)
    {
        return drinkRepository.save(drink);
    }

    @Cacheable(value = "drinkCache", key = "'AllDrinks'")
    public Map<String, Drink> findAllDrinks()
    {
        return drinkRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(Drink::getIdAsString, Function.identity()));
    }

    @Cacheable(value = "drinkCache", key = "'drink'+#id")
    public Drink findDrinkById(Long id)
    {
        return drinkRepository
                .findDrinkById(id)
                .orElseThrow(()->new DrinkNotFoundException(id));
    }

    public Drink findDrinkByName(String name)
    {
        return drinkRepository
                .findByName(name)
                .orElseThrow(()->new DrinkNotFoundException(name));
    }

    public Map<String, Drink> findDrinkWithCaloriesCriteria(Long caloriesLimit, String criteria)
    {
        switch (criteria){
            case ComparationType.lessThanEqual:
                return drinkRepository
                        .findByCaloriesLessThanEqual(caloriesLimit)
                        .stream()
                        .collect(Collectors.toMap(Drink::getIdAsString, Function.identity()));
            case ComparationType.lessThan:
                return drinkRepository
                        .findByCaloriesLessThan(caloriesLimit)
                        .stream()
                        .collect(Collectors.toMap(Drink::getIdAsString, Function.identity()));
            case ComparationType.greaterThan:
                return drinkRepository
                        .findByCaloriesGreaterThan(caloriesLimit)
                        .stream()
                        .collect(Collectors.toMap(Drink::getIdAsString, Function.identity()));
            case ComparationType.greaterThanEqual:
                return drinkRepository
                        .findByCaloriesGreaterThanEqual(caloriesLimit)
                        .stream()
                        .collect(Collectors.toMap(Drink::getIdAsString, Function.identity()));
            default:
                return null;
        }
    }

    public Drink updateDrink(Drink drink, Long id)
    {
        Optional<Drink> drinkFromDb = drinkRepository.findDrinkById(id);
        drinkFromDb.ifPresent(theDrink -> {
            theDrink.setName(drink.getName());
            theDrink.setCalories(drink.getCalories());
            theDrink.setImageUrl(drink.getImageUrl());
            drinkRepository.save(theDrink);
        });
        return drinkFromDb.stream().findFirst().orElse(null);
    }

    public void deleteDrink(Long id)
    {
        drinkRepository.deleteDrinkById(id);
    }
}
