package tech.kresnofatih.foodmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.kresnofatih.foodmanager.models.AppUser;
import tech.kresnofatih.foodmanager.security.AuthRequest;
import tech.kresnofatih.foodmanager.security.AuthResponse;

import java.util.Objects;

@Service
public class AuthService {
    private final JwtService jwtService;

    private final AppUserService appUserService;

    @Autowired
    public AuthService(JwtService jwtService, AppUserService appUserService) {
        this.jwtService = jwtService;
        this.appUserService = appUserService;
    }

    public AuthResponse getToken(AuthRequest authRequest)
    {
        String token = null;
        AppUser foundUser = appUserService.findAppUserByUsername(authRequest.getUsername());
        if(!Objects.isNull(foundUser))
        {
            boolean passwordIsMatch = (foundUser.getPassword().equals(authRequest.getPassword()));
            if(passwordIsMatch)
            {
                token = jwtService.createToken(foundUser.getId(), foundUser.getUsername(), foundUser.getRole());
            }
        }
        return new AuthResponse(token);
    }
}
