package tech.kresnofatih.foodmanager.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tech.kresnofatih.foodmanager.services.JwtService;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtService jwtService;

    public SecurityConfiguration(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtAuthFilter(authenticationManager(), jwtService))
                .addFilterAfter(new JwtVerifierFilter(jwtService), JwtAuthFilter.class)
                .authorizeRequests()
                .antMatchers("/player/**").hasRole("ADMIN")
                .antMatchers("/food/**").hasAnyRole("ADMIN","USER")
                .antMatchers("/drink/**").permitAll()
                .antMatchers("/appuser/**").permitAll()
                .antMatchers("/auth/**").permitAll();
//                .and().httpBasic();
//                .and().formLogin()
    }

    @Bean
    public PasswordEncoder getPasswordEncoder()
    {
        return NoOpPasswordEncoder.getInstance();
    }
}
