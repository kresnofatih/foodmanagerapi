package tech.kresnofatih.foodmanager.security;

public class AuthResponse {
    private String jwt;

    public AuthResponse(){}

    public AuthResponse(String jwt)
    {
        this.jwt = jwt;
    }

    // getter
    public String getJwt() {
        return jwt;
    }

    // setter
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
