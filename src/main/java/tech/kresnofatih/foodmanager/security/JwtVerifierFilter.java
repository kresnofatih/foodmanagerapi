package tech.kresnofatih.foodmanager.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import tech.kresnofatih.foodmanager.services.JwtService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class JwtVerifierFilter extends OncePerRequestFilter {
    private final JwtService jwtService;

    public JwtVerifierFilter(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        String authHeader = httpServletRequest.getHeader("Authorization");
        if(!Objects.isNull(authHeader) && authHeader.startsWith("Bearer "))
        {
            String token = authHeader.replace("Bearer ", "");
            boolean tokenIsValid = jwtService.validateToken(token);
            String roleFromToken = jwtService.extractClaim(token, "rle");
            var authorities = getAuths(roleFromToken);
            if(tokenIsValid)
            {
                Authentication authentication = new UsernamePasswordAuthenticationToken(
                        null,
                        null,
                        authorities
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private List<SimpleGrantedAuthority> getAuths(String roleFromToken)
    {
        var res = new ArrayList<SimpleGrantedAuthority>();
        res.add(new SimpleGrantedAuthority("ROLE_"+roleFromToken));
        return res;
    }
}
