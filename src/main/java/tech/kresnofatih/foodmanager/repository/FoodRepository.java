package tech.kresnofatih.foodmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.kresnofatih.foodmanager.models.Food;

import javax.transaction.Transactional;
import java.util.Optional;

public interface FoodRepository extends JpaRepository<Food, Long> {
    @Transactional
    void deleteFoodById(Long id);

    Optional<Food> findFoodById(Long id);
}
