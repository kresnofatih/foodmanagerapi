package tech.kresnofatih.foodmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.kresnofatih.foodmanager.models.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByUsername(String username);

    Optional<AppUser> findAppUserById(Long id);
}
