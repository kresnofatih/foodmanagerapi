package tech.kresnofatih.foodmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.kresnofatih.foodmanager.models.Drink;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface DrinkRepository extends JpaRepository<Drink, String> {

    Optional<Drink> findDrinkById(Long id);

    @Transactional
    void deleteDrinkById(Long id);

    Optional<Drink> findByName(String name);

    List<Drink> findByCaloriesGreaterThanEqual(Long caloriesLimit);

    List<Drink> findByCaloriesGreaterThan(Long caloriesLimit);

    List<Drink> findByCaloriesLessThan(Long caloriesLimit);

    List<Drink> findByCaloriesLessThanEqual(Long caloriesLimit);
}
