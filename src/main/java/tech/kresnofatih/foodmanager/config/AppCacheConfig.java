package tech.kresnofatih.foodmanager.config;

import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

import static org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair;

@Configuration
public class AppCacheConfig {

    @Bean
    public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer()
    {
        return builder -> builder
                .withCacheConfiguration(
                        "drinkCache",
                        RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(3))
                )
                .withCacheConfiguration(
                        "appuserCache",
                        RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(3))
                );
    }

    @Bean
    public RedisCacheConfiguration cacheConfiguration()
    {
        return RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofMinutes(15))
                .disableCachingNullValues()
                .serializeValuesWith(SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory()
    {
        RedisStandaloneConfiguration redisStdConfig = new RedisStandaloneConfiguration("localhost", 6379);
//        redisStdConfig.setPassword("pwd123");
        return new JedisConnectionFactory(redisStdConfig);
    }

    @Bean
    public RedisTemplate<Long, Object> redisTemplate()
    {
        RedisTemplate<Long, Object> baseRedis = new RedisTemplate<>();
        baseRedis.setConnectionFactory(jedisConnectionFactory());
        baseRedis.setKeySerializer(new GenericToStringSerializer<>(Long.class));
        return baseRedis;
    }
}
