package tech.kresnofatih.foodmanager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/drink/**")
                .allowedMethods("GET")
                .allowedHeaders("*")
                .allowedOrigins("http://localhost:3000")
                .allowCredentials(false)
                .maxAge(3600);
    }
}
