package tech.kresnofatih.foodmanager.utility;

import java.util.Calendar;
import java.util.Date;

public class DateTimeUtility {
    public DateTimeUtility(){}

    public Date getDateAddHoursFromNow(int hours)
    {
        Calendar calendar = Calendar.getInstance();
        Date now = new Date();  //get current time
        calendar.setTime(now);  //set calendar time to now
        calendar.add(Calendar.HOUR_OF_DAY, hours);  //add hours to calendar time
        return calendar.getTime();
    }

    public Date getDateAddMinutesFromNow(int minutes)
    {
        Calendar calendar = Calendar.getInstance();
        Date now = new Date();
        calendar.setTime(now);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }

    public Date getDateNow()
    {
        Calendar calendar = Calendar.getInstance();
        Date now = new Date();
        calendar.setTime(now);
        return calendar.getTime();
    }
}
